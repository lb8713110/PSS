<?php
// W skrypcie definicji kontrolera nie trzeba dołączać problematycznego skryptu config.php,
// ponieważ będzie on użyty w miejscach, gdzie config.php zostanie już wywołany.

require_once $conf->root_path.'/lib/smarty/Smarty.class.php';
require_once $conf->root_path.'/lib/Messages.class.php';
require_once $conf->root_path.'/app/calc/CalcForm.class.php';
require_once $conf->root_path.'/app/calc/CalcResult.class.php';

class CalcCtrl {

	private $msgs;   //wiadomości dla widoku
	private $infos;  //informacje dla widoku
	private $form;   //dane formularza (do obliczeń i dla widoku)
	private $result; //inne dane dla widoku
	private $hide_intro; //zmienna informująca o tym czy schować intro

	/** 
	 * Konstruktor - inicjalizacja właściwości
	 */
	public function __construct(){
		//stworzenie potrzebnych obiektów
		$this->msgs = new Messages();
		$this->form = new CalcForm();
		$this->result = new CalcResult();
		$this->hide_intro = false;
	}
	
	/** 
	 * Pobranie parametrów
	 */
	public function getParams(){
        $this->form->how_many_years = isset($_REQUEST['how_many_years']) ? $_REQUEST['how_many_years'] : null;
        $this->form->amount = isset($_REQUEST['amount']) ? $_REQUEST['amount'] : null;
        $this->form->percentage = isset($_REQUEST['percentage']) ? $_REQUEST['percentage'] : null;

	}
	
	/** 
	 * Walidacja parametrów
	 * @return true jeśli brak błedów, false w przeciwnym wypadku 
	 */
	public function validate() {
		// sprawdzenie, czy parametry zostały przekazane
        if (! (isset ( $this->form->how_many_years ) && isset ( $this->form->amount ) && isset ( $this->form->percentage ))) {
            // sytuacja wystąpi kiedy np. kontroler zostanie wywołany bezpośrednio - nie z formularza
            return false; //zakończ walidację z błędem
        } else {
            $this->hide_intro = true; //przyszły pola formularza, więc - schowaj wstęp
        }

        // sprawdzenie, czy potrzebne wartości zostały przekazane
        if ($this->form->how_many_years == "") {
            $this->msgs->addError('Nie podano lat');
        }
        if ($this->form->amount == "") {
            $this->msgs->addError('Nie podano kwoty');
        }
        if ($this->form->percentage == "") {
            $this->msgs->addError('Nie podano oprocentowania');
        }

        // nie ma sensu walidować dalej gdy brak parametrów
        if (! $this->msgs->isError()) {

            // sprawdzenie, czy $x i $y są liczbami całkowitymi
            if (! is_numeric ( $this->form->how_many_years )) {
                $this->msgs->addError('Lata nie są liczbą!');
            }

            if (! is_numeric ( $this->form->amount )) {
                $this->msgs->addError('Kwota nie jest liczbą!');
            }

            if (! is_numeric ( $this->form->percentage )) {
                $this->msgs->addError('Procenty nie są liczbą!');
            }
        }

        return ! $this->msgs->isError();
    }
	
	/** 
	 * Pobranie wartości, walidacja, obliczenie i wyświetlenie
	 */
	public function process(){

		$this->getparams();
		
		if ($this->validate()) {
				
			//konwersja parametrów na int
            $this->form->how_many_years = intval($this->form->how_many_years);
            $this->form->amount = floatval($this->form->amount);
            $this->form->percentage = floatval($this->form->percentage);
            $this->msgs->addInfo('Parametry poprawne.');

            //wykonanie operacji
            $this->result->result = ($this->form->amount * (100+$this->form->percentage)/100) / ($this->form->how_many_years*12);
            $this->msgs->addInfo('Wykonano obliczenia.');
        }

        $this->generateView();
    }

	
	
	/**
	 * Wygenerowanie widoku
	 */
	public function generateView(){
		global $conf;
		
		$smarty = new Smarty();
		$smarty->assign('conf',$conf);
		
		$smarty->assign('page_title','Kalkulator kredytowy 06');
        $smarty->assign('page_description','Obiektowość. Funkcjonalność aplikacji zamknięta w metodach różnych obiektów. Pełen model MVC.');
		$smarty->assign('page_header','Twoja ratka ma teraz kontroler główny');
				
		$smarty->assign('hide_intro',$this->hide_intro);
		
		$smarty->assign('msgs',$this->msgs);
		$smarty->assign('form',$this->form);
		$smarty->assign('res',$this->result);
		
		$smarty->display($conf->root_path.'/app/calc/CalcView.tpl');
	}
}
