<?php

use core\App;
use core\Utils;

App::getRouter()->setDefaultRoute('homePage'); // akcja/ścieżka domyślna
App::getRouter()->setLoginRoute('login'); // akcja/ścieżka na potrzeby logowania (przekierowanie, gdy nie ma dostępu)

Utils::addRoute('homePage',    'HomePage');
Utils::addRoute('register', 'RegistrationCtrl');

Utils::addRoute('personList',    'PersonListCtrl', ['admin']);
Utils::addRoute('personNew',     'PersonEditCtrl',	['admin']);
Utils::addRoute('personEdit',    'PersonEditCtrl',	['admin']);
Utils::addRoute('personSave',    'PersonEditCtrl',	['admin']);
Utils::addRoute('personDelete',  'PersonEditCtrl',	['admin']);

Utils::addRoute('itemList',    'StockListCtrl', ['admin', 'user', 'magazynier']);
Utils::addRoute('itemCheckIn',    'StockCheckInCtrl', ['magazynier']);
Utils::addRoute('itemCheckInConfirmation',    'StockCheckInCtrl', ['magazynier']);
Utils::addRoute('itemCheckOutConfirmation',    'StockCheckOutCtrl', ['magazynier']);

Utils::addRoute('itemCheckOut',    'StockCheckOutCtrl', ['magazynier']);
Utils::addRoute('stockItemNew',    'StockItemNewCtrl', ['magazynier']);
Utils::addRoute('stockItemNewConfirm',    'StockItemNewCtrl', ['magazynier']);

Utils::addRoute('locationList',    'LocationListCtrl', ['magazynier']);
Utils::addRoute('locationEdit',    'LocationEditCtrl', ['magazynier']);
Utils::addRoute('locationSave',    'LocationEditCtrl', ['magazynier']);
Utils::addRoute('locationNew',    'LocationNewCtrl', ['magazynier']);
Utils::addRoute('locationDelete',    'LocationEditCtrl', ['magazynier']);

Utils::addRoute('itemEdit',    'StockItemEditCtrl', ['magazynier']);
Utils::addRoute('itemEditConfirmation',    'StockItemEditCtrl', ['magazynier']);
Utils::addRoute('itemEdit',    'StockItemEditCtrl', [ 'magazynier']);
Utils::addRoute('itemDelete',    'StockItemEditCtrl', ['magazynier']);

Utils::addRoute('loginShow',     'LoginCtrl');
Utils::addRoute('login',         'LoginCtrl');
Utils::addRoute('logout',        'LoginCtrl');
