<?php
/* Smarty version 4.5.1, created on 2024-05-06 21:46:32
  from 'C:\xampp\htdocs\php_09_bd\app\views\PersonEdit.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '4.5.1',
  'unifunc' => 'content_66393398628c02_13115914',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '581ee4f7b5d2e796911f03ec39cd1d7998e5519a' => 
    array (
      0 => 'C:\\xampp\\htdocs\\php_09_bd\\app\\views\\PersonEdit.tpl',
      1 => 1715024786,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_66393398628c02_13115914 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5420297946639339860fe82_62308206', 'top');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, "main.tpl");
}
/* {block 'top'} */
class Block_5420297946639339860fe82_62308206 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'top' => 
  array (
    0 => 'Block_5420297946639339860fe82_62308206',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\xampp\\htdocs\\php_09_bd\\lib\\smarty\\plugins\\function.html_checkboxes.php','function'=>'smarty_function_html_checkboxes',),));
?>


<div class="bottom-margin">
<form action="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
personSave" method="post" class="pure-form pure-form-aligned">
	<fieldset>
		<legend>Dane osoby</legend>
		<div class="pure-control-group">
            <label for="name">imię</label>
            <input id="name" type="text" placeholder="imię" name="name" value="<?php echo $_smarty_tpl->tpl_vars['form']->value->name;?>
">
        </div>
		<div class="pure-control-group">
            <label for="surname">nazwisko</label>
            <input id="surname" type="text" placeholder="nazwisko" name="surname" value="<?php echo $_smarty_tpl->tpl_vars['form']->value->surname;?>
">
        </div>
		<div class="pure-control-group">
            <label for="username">login</label>
            <input id="username" type="text" placeholder="login" name="username" value="<?php echo $_smarty_tpl->tpl_vars['form']->value->username;?>
">
        </div>

        <?php echo smarty_function_html_checkboxes(array('id'=>"checkboxes",'name'=>'roles','values'=>$_smarty_tpl->tpl_vars['roles_ids']->value,'output'=>$_smarty_tpl->tpl_vars['roles_names']->value,'selected'=>$_smarty_tpl->tpl_vars['default_role_id']->value,'separator'=>'<br />'),$_smarty_tpl);?>

        <div class="pure-controls">
            <input type="submit" class="pure-button pure-button-primary" value="Zapisz"/>
            <a class="pure-button button-secondary" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
personList">Powrót</a>
        </div>
    </fieldset>
    <input type="hidden" name="username" value="<?php echo $_smarty_tpl->tpl_vars['form']->value->username;?>
">

</form>

</div>

<?php
}
}
/* {/block 'top'} */
}
