<?php
/* Smarty version 4.5.1, created on 2024-05-14 16:55:02
  from 'C:\xampp\htdocs\php_09_bd\app\views\StockCheckOut.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '4.5.1',
  'unifunc' => 'content_66437b46cf1123_42361695',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '81fbcb0bc3a55dea9cfb1f15b8d2e6d34d999ef8' => 
    array (
      0 => 'C:\\xampp\\htdocs\\php_09_bd\\app\\views\\StockCheckOut.tpl',
      1 => 1715698493,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_66437b46cf1123_42361695 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_109813304166437b46cdb3c5_46737317', 'top');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_151231649366437b46cdc9b8_51362974', 'bottom');
?>


<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, "main.tpl");
}
/* {block 'top'} */
class Block_109813304166437b46cdb3c5_46737317 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'top' => 
  array (
    0 => 'Block_109813304166437b46cdb3c5_46737317',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>




<?php
}
}
/* {/block 'top'} */
/* {block 'bottom'} */
class Block_151231649366437b46cdc9b8_51362974 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'bottom' => 
  array (
    0 => 'Block_151231649366437b46cdc9b8_51362974',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\xampp\\htdocs\\php_09_bd\\lib\\smarty\\plugins\\function.html_options.php','function'=>'smarty_function_html_options',),));
?>




    <table id="tab_items" class="pure-table pure-table-bordered">
        <thead>
        <tr>


            <th>id</th>
            <th>nazwa</th>
            <th>Z lokacji</th>
        </tr>
        </thead>
        <tbody>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['items']->value, 'p');
$_smarty_tpl->tpl_vars['p']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->do_else = false;
?>
            <tr><td><?php echo $_smarty_tpl->tpl_vars['p']->value["item_id"];?>
</td><td><?php echo $_smarty_tpl->tpl_vars['p']->value["item_name"];?>
</td><td><?php echo $_smarty_tpl->tpl_vars['p']->value["item_default_location_name"];?>
</td></tr>
        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>


        </tbody>
    </table>
<form action="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
itemCheckOutConfirmation" method="post" class="pure-form pure-form-aligned">

<br>
    Użytkownik pobierający towar:
    <br>

    <select name="customer_id" >
            <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['cust_ids']->value,'output'=>$_smarty_tpl->tpl_vars['cust_names']->value),$_smarty_tpl);?>

        </select>
    <br>
    <br>
    Lokacja gdzie towar trafi:
    <br>

    <select name="location_id" >
        <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['location_ids']->value,'output'=>$_smarty_tpl->tpl_vars['location_names']->value),$_smarty_tpl);?>

    </select>
    <br>
    <br>


    <div class="pure-controls">
        <input type="hidden" name="item_id" value="<?php echo $_smarty_tpl->tpl_vars['items']->value[0]['item_id'];?>
">

        <input type="submit" class="pure-button button-checkout" value="Wydaj towar"/>
        <a class="pure-button button-secondary" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
itemList">Powrót</a>
    </div>
<?php
}
}
/* {/block 'bottom'} */
}
