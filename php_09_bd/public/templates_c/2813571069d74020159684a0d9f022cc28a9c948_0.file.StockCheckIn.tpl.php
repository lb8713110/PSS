<?php
/* Smarty version 4.5.1, created on 2024-05-13 23:56:46
  from 'C:\xampp\htdocs\php_09_bd\app\views\StockCheckIn.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '4.5.1',
  'unifunc' => 'content_66428c9e32f318_83726487',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2813571069d74020159684a0d9f022cc28a9c948' => 
    array (
      0 => 'C:\\xampp\\htdocs\\php_09_bd\\app\\views\\StockCheckIn.tpl',
      1 => 1715637405,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_66428c9e32f318_83726487 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_51108473166428c9e31cdb2_77187212', 'top');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_183389581566428c9e3216f8_37997760', 'bottom');
?>


<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, "main.tpl");
}
/* {block 'top'} */
class Block_51108473166428c9e31cdb2_77187212 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'top' => 
  array (
    0 => 'Block_51108473166428c9e31cdb2_77187212',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


    <div class="bottom-margin">
        <form class="pure-form pure-form-stacked" action="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_url;?>
itemList">
            <legend>Opcje wyszukiwania</legend>
            <fieldset>
                <input type="text" placeholder="nazwa" name="sf_name" value="<?php echo $_smarty_tpl->tpl_vars['searchForm']->value->item_name;?>
" /><br />
                <button type="submit" class="pure-button pure-button-primary">Filtruj</button>
            </fieldset>
        </form>
    </div>

<?php
}
}
/* {/block 'top'} */
/* {block 'bottom'} */
class Block_183389581566428c9e3216f8_37997760 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'bottom' => 
  array (
    0 => 'Block_183389581566428c9e3216f8_37997760',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>




    <table id="tab_items" class="pure-table pure-table-bordered">
        <thead>
        <tr>


            <th>id</th>
            <th>nazwa</th>

            <th>Zdający użytkownik</th>
            <th>do lokacji</th>
        </tr>
        </thead>
        <tbody>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['items']->value, 'p');
$_smarty_tpl->tpl_vars['p']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->do_else = false;
?>
            <tr><td><?php echo $_smarty_tpl->tpl_vars['p']->value["item_id"];?>
</td><td><?php echo $_smarty_tpl->tpl_vars['p']->value["item_name"];?>
</td><td><?php echo $_smarty_tpl->tpl_vars['p']->value["user_id_surname"];?>
</td><td><?php echo $_smarty_tpl->tpl_vars['p']->value["item_default_location_name"];?>
</td></tr>
        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>


        </tbody>
    </table>
    <br>
    <div class="pure-controls">

        <a class="pure-button button-success" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
itemCheckInConfirmation/<?php echo $_smarty_tpl->tpl_vars['items']->value[0]['item_id'];?>
">Zdaj towar</a>
    </div>
<?php
}
}
/* {/block 'bottom'} */
}
