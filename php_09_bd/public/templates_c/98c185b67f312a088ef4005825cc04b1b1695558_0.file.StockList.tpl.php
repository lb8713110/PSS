<?php
/* Smarty version 4.5.1, created on 2024-05-13 23:00:49
  from 'C:\xampp\htdocs\php_09_bd\app\views\StockList.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '4.5.1',
  'unifunc' => 'content_66427f81b8d935_98768423',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '98c185b67f312a088ef4005825cc04b1b1695558' => 
    array (
      0 => 'C:\\xampp\\htdocs\\php_09_bd\\app\\views\\StockList.tpl',
      1 => 1715634047,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_66427f81b8d935_98768423 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14497283866427f81b76bc8_61382892', 'top');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_99604257166427f81b7bed2_40845147', 'bottom');
?>


<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, "main.tpl");
}
/* {block 'top'} */
class Block_14497283866427f81b76bc8_61382892 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'top' => 
  array (
    0 => 'Block_14497283866427f81b76bc8_61382892',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


    <div class="bottom-margin">
        <form class="pure-form pure-form-stacked" action="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_url;?>
itemList">
            <legend>Opcje wyszukiwania</legend>
            <fieldset>
                <input type="text" placeholder="nazwa" name="sf_name" value="<?php echo $_smarty_tpl->tpl_vars['searchForm']->value->item_name;?>
" /><br />
                <button type="submit" class="pure-button pure-button-primary">Filtruj</button>
            </fieldset>
        </form>
    </div>

<?php
}
}
/* {/block 'top'} */
/* {block 'bottom'} */
class Block_99604257166427f81b7bed2_40845147 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'bottom' => 
  array (
    0 => 'Block_99604257166427f81b7bed2_40845147',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


    <div class="bottom-margin">
        <a class="pure-button button-success" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
personNew">+ Nowa osoba</a>
    </div>

    <table id="tab_people" class="pure-table pure-table-bordered">
        <thead>
        <tr>


            <th>id</th>
            <th>nazwa</th>
            <th>model</th>
            <th>użytkownik</th>
            <th>lokacja</th>
            <th>lokacja standardowa</th>
        </tr>
        </thead>
        <tbody>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['items']->value, 'p');
$_smarty_tpl->tpl_vars['p']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->do_else = false;
?>
            <tr><td><?php echo $_smarty_tpl->tpl_vars['p']->value["item_id"];?>
</td><td><?php echo $_smarty_tpl->tpl_vars['p']->value["item_name"];?>
</td><td><?php echo $_smarty_tpl->tpl_vars['p']->value["item_model_name"];?>
</td><td><?php echo $_smarty_tpl->tpl_vars['p']->value["user_id_surname"];?>
</td><td><?php echo $_smarty_tpl->tpl_vars['p']->value["item_location_name"];?>
</td><td><?php echo $_smarty_tpl->tpl_vars['p']->value["item_default_location_name"];?>
</td><td><a class="button-small pure-button button-secondary" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_url;?>
itemEdit/<?php echo $_smarty_tpl->tpl_vars['p']->value['item_id'];?>
">Edytuj</a>&nbsp;<a class="button-small pure-button button-warning" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_url;?>
itemDelete/<?php echo $_smarty_tpl->tpl_vars['p']->value['item_id'];?>
">Usuń</a>&nbsp;<?php if (empty($_smarty_tpl->tpl_vars['p']->value["user_id_surname"])) {?><a class="button-small pure-button button-checkout" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_url;?>
itemCheckOut/<?php echo $_smarty_tpl->tpl_vars['p']->value['item_id'];?>
">Przydziel</a><?php } else { ?><a class="button-small pure-button button-checkin" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_url;?>
itemCheckIn/<?php echo $_smarty_tpl->tpl_vars['p']->value['item_id'];?>
">Zwróć</a><?php }?></td></tr>
        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </tbody>
    </table>

<?php
}
}
/* {/block 'bottom'} */
}
