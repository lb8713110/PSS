{extends file="main.tpl"}

{block name=top}

    <div class="bottom-margin">
        <form class="pure-form pure-form-stacked" action="{$conf->action_url}locationList">
            <legend>Opcje wyszukiwania</legend>
            <fieldset>
                <input type="text" placeholder="nazwa" name="sf_name" value="{$searchForm->location_name}" /><br />
                <button type="submit" class="pure-button pure-button-primary">Filtruj</button>
            </fieldset>
        </form>
    </div>

{/block}

{block name=bottom}

    <div class="bottom-margin">
        <a class="pure-button button-success" href="{$conf->action_root}locationNew">+ Nowa lokacja</a>
    </div>

    <table id="tab_people" class="pure-table pure-table-bordered">
        <thead>
        <tr>


            <th>id</th>
            <th>nazwa lokacji</th>
            <th>Akcje</th>
        </tr>
        </thead>
        <tbody>
        {foreach $locations as $p}
            {strip}
                <tr>
                    <td>{$p["location_id"]}</td>
                    <td>{$p["location_name"]}</td>

                    <td>
                        <a class="button-small pure-button button-secondary" href="{$conf->action_url}locationEdit/{$p['location_id']}">Edytuj</a>
                        &nbsp;
                        <a class="button-small pure-button button-warning" href="{$conf->action_url}locationDelete/{$p['location_id']}">Usuń</a>
                        &nbsp;


                    </td>
                </tr>
            {/strip}
        {/foreach}
        </tbody>
    </table>

{/block}

