{extends file="main.tpl"}

{block name=top}
<form action="{$conf->action_root}locationNew" method="post" class="pure-form pure-form-aligned bottom-margin">
    <legend>Rejestracja nowego użytkownika</legend>
    <fieldset>
        <div class="pure-control-group">
            <label for="name">Nazwa lokacji: </label>
            <input id="name" type="text" name="name" value="{$form->name}"/>
        </div>

        <div class="pure-controls">
            <input type="submit" value="Dodaj lokację" class="pure-button pure-button-primary button-success"/>
        </div>
    </fieldset>
</form>
{/block}
