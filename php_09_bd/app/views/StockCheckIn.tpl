{extends file="main.tpl"}

{block name=top}

    <div class="bottom-margin">
        <form class="pure-form pure-form-stacked" action="{$conf->action_url}itemList">
            <legend>Opcje wyszukiwania</legend>
            <fieldset>
                <input type="text" placeholder="nazwa" name="sf_name" value="{$searchForm->item_name}" /><br />
                <button type="submit" class="pure-button pure-button-primary">Filtruj</button>
            </fieldset>
        </form>
    </div>

{/block}

{block name=bottom}



    <table id="tab_items" class="pure-table pure-table-bordered">
        <thead>
        <tr>


            <th>id</th>
            <th>nazwa</th>

            <th>Zdający użytkownik</th>
            <th>do lokacji</th>
        </tr>
        </thead>
        <tbody>
        {foreach $items as $p}
            {strip}
                <tr>
                    <td>{$p["item_id"]}</td>
                    <td>{$p["item_name"]}</td>
                    <td>{$p["user_id_surname"]}</td>
                    <td>{$p["item_default_location_name"]}</td>


                </tr>
            {/strip}
        {/foreach}


        </tbody>
    </table>
    <br>
    <div class="pure-controls">

        <a class="pure-button button-success" href="{$conf->action_root}itemCheckInConfirmation/{$items[0]['item_id']}">Zdaj towar</a>
    </div>
{/block}

