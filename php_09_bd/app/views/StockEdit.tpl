{extends file="main.tpl"}

{block name=top}
{/block}

{block name=bottom}

<form action="{$conf->action_root}itemEditConfirmation" method="post" class="pure-form pure-form-aligned bottom-margin">
    <fieldset>

    <table id="tab_people" class="pure-table pure-table-bordered">
        <thead>
        <tr>


            <th>id</th>
            <th>nazwa</th>
            <th>model</th>
            <th>użytkownik</th>
            <th>lokacja</th>
            <th>lokacja standardowa</th>
        </tr>
        </thead>
        <tbody>
        {foreach $items as $p}
            {strip}
                <tr>
                    <td>{$p["item_id"]}</td>
                    <td><div >
                            <input id="item_name" type="text" name="item_name" placeholder="nazwa" value="{$p["item_name"]}"/>
                        </div></td>
                    <td><select name="model_id" >
                            {html_options values=$model_ids output=$model_names selected=$selected_model_id}
                        </select></td>
                    <td>{$p["user_id_surname"]}</td>
                    <td>{$p["item_location_name"]}</td>
                    <td><select name="location_id" >
                            {html_options values=$location_ids output=$location_names selected=$selected_location_id}
                        </select></td>
                    <td>
                        <input type="hidden" name="item_id" value={$p["item_id"]}>
                        <input type="submit" value="Zmień" class="button-small pure-button button-secondary">
                        &nbsp;

                        &nbsp;
                        {if empty($p["user_id_surname"])}
                            <a class="button-small pure-button button-checkout" href="{$conf->action_url}itemDelete/{$p['item_id']}">Usuń</a>
                        {else}
                            <a class="button-small pure-button button-checkout" href="{$conf->action_url}itemDelete/{$p['item_id']}" disabled>Usuń</a>
                        {/if}

                    </td>
                </tr>

            {/strip}
        {/foreach}
        </tbody>
    </table>
    </fieldset>
</form>

{/block}

