{extends file="main.tpl"}

{block name=top}



{/block}

{block name=bottom}



    <table id="tab_items" class="pure-table pure-table-bordered">
        <thead>
        <tr>


            <th>id</th>
            <th>nazwa</th>
            <th>Z lokacji</th>
        </tr>
        </thead>
        <tbody>
        {foreach $items as $p}
            {strip}
                <tr>
                    <td>{$p["item_id"]}</td>
                    <td>{$p["item_name"]}</td>
                    <td>{$p["item_default_location_name"]}</td>


                </tr>
            {/strip}
        {/foreach}


        </tbody>
    </table>
<form action="{$conf->action_root}itemCheckOutConfirmation" method="post" class="pure-form pure-form-aligned">

<br>
    Użytkownik pobierający towar:
    <br>

    <select name="customer_id" >
            {html_options values=$cust_ids output=$cust_names}
        </select>
    <br>
    <br>
    Lokacja gdzie towar trafi:
    <br>

    <select name="location_id" >
        {html_options values=$location_ids output=$location_names}
    </select>
    <br>
    <br>


    <div class="pure-controls">
        <input type="hidden" name="item_id" value="{$items[0]['item_id']}">

        <input type="submit" class="pure-button button-checkout" value="Wydaj towar"/>
        <a class="pure-button button-secondary" href="{$conf->action_root}itemList">Powrót</a>
    </div>
{/block}

