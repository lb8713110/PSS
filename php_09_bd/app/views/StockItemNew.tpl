{extends file="main.tpl"}

{block name=top}
<form action="{$conf->action_root}stockItemNewConfirm" method="post" class="pure-form pure-form-aligned bottom-margin">
    <legend>Dodanie nowego towaru</legend>
    <fieldset>
        <div class="pure-control-group">
            <label for="name">Nazwa towaru: </label>
            <input id="name" type="text" name="name" placeholder="nazwa"/>
        </div>

        <div class="pure-control-group">
            <label for="name">Standardowa lokacja towaru: </label>
            <select name="location_id" >
            {html_options values=$location_ids output=$location_names}
        </select>
        </div>

        <div class="pure-control-group">
            <label for="name">Model towaru: </label>
            <select name="model_id" >
            {html_options values=$model_ids output=$model_names}
        </select>
        <br>
        </div>

        <div class="pure-controls">
            <input type="submit" value="Dodaj" class="pure-button button-success"/>
        </div>
    </fieldset>
</form>
{/block}
