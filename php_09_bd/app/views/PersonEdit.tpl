{extends file="main.tpl"}

{block name=top}

<div class="bottom-margin">
<form action="{$conf->action_root}personSave" method="post" class="pure-form pure-form-aligned">
	<fieldset>
		<legend>Dane osoby</legend>
		<div class="pure-control-group">
            <label for="name">imię</label>
            <input id="name" type="text" placeholder="imię" name="name" value="{$form->name}">
        </div>
		<div class="pure-control-group">
            <label for="surname">nazwisko</label>
            <input id="surname" type="text" placeholder="nazwisko" name="surname" value="{$form->surname}">
        </div>
		<div class="pure-control-group">
            <label for="username">login</label>
            <input id="username" type="text" placeholder="login" name="username" value="{$form->username}">
        </div>

        {html_checkboxes id="checkboxes" name='roles' values=$roles_ids output=$roles_names
        selected=$default_role_id separator='<br />'}
        <div class="pure-controls">
            <input type="submit" class="pure-button pure-button-primary" value="Zapisz"/>
            <a class="pure-button button-secondary" href="{$conf->action_root}personList">Powrót</a>
        </div>
    </fieldset>
    <input type="hidden" name="username" value="{$form->username}">

</form>

</div>

{/block}
