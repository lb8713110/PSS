{extends file="main.tpl"}

{block name=top}

    <div class="bottom-margin">
        <form class="pure-form pure-form-stacked" action="{$conf->action_url}itemList">
            <legend>Opcje wyszukiwania</legend>
            <fieldset>
                <input type="text" placeholder="nazwa" name="sf_name" value="{$searchForm->item_name}" /><br />
                <button type="submit" class="pure-button pure-button-primary">Filtruj</button>
            </fieldset>
        </form>
    </div>

{/block}

{block name=bottom}

    <div class="bottom-margin">
        <a class="pure-button button-success" href="{$conf->action_root}stockItemNew">+ Nowy element magazynowy</a>
    </div>

    <table id="tab_people" class="pure-table pure-table-bordered">
        <thead>
        <tr>


            <th>id</th>
            <th>nazwa</th>
            <th>model</th>
            <th>użytkownik</th>
            <th>lokacja</th>
            <th>lokacja standardowa</th>
            <th>akcja</th>

        </tr>
        </thead>
        <tbody>
        {foreach $items as $p}
            {strip}
                <tr>
                    <td>{$p["item_id"]}</td>
                    <td>{$p["item_name"]}</td>
                    <td>{$p["item_model_name"]}</td>
                    <td>{$p["user_id_surname"]}</td>

                    <td>
                        {if !empty($p["user_id_surname"])}
                        {$p["item_location_name"] }

                        {else}
                        &nbsp
                    {/if}
                    </td>
                    <td>{$p["item_default_location_name"]}</td>
                    <td>
                        <a class="button-small pure-button button-secondary" href="{$conf->action_url}itemEdit/{$p['item_id']}">Edytuj</a>
                        &nbsp;
                        <a class="button-small pure-button button-warning" href="{$conf->action_url}itemDelete/{$p['item_id']}">Usuń</a>
                        &nbsp;
                        {if empty($p["user_id_surname"])}
                        <a class="button-small pure-button button-checkout" href="{$conf->action_url}itemCheckOut/{$p['item_id']}">Przydziel</a>
                        {else}
                            <a class="button-small pure-button button-checkin" href="{$conf->action_url}itemCheckIn/{$p['item_id']}">Zwróć</a>
                        {/if}

                    </td>
                </tr>
            {/strip}
        {/foreach}
        </tbody>
    </table>

{/block}

