<?php

namespace app\forms;

class PersonEditForm {
	public $id;
	public $name;
	public $surname;
	public $username;
    public $password;
    public $role_checkboxes;
    public $_SESSION;
}