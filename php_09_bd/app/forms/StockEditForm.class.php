<?php

namespace app\forms;

class StockEditForm {
	public $item_name;
    public $item_id;
    public $item_location_name;
    public $item_model_name;
    public $item_default_location_name;
    public $item_selected_default_location_id;
    public $item_default_location_id;
    public $user_id_surname;
    public $user_id;
    public $item_model_id;
    public $item_selected_model_id;
} 