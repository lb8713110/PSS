<?php

namespace app\controllers;

use app\forms\StockCheckOutForm;
use core\App;
use core\Utils;
use core\ParamUtils;
use app\forms\StockSearchForm;
use Medoo\Medoo;

class StockCheckOutCtrl {

    private $form; //dane formularza wyszukiwania
    private $records; //rekordy pobrane z bazy danych

    public function __construct() {
        //stworzenie potrzebnych obiektów
        $this->form = new StockSearchForm();
        $this->form_checkout = new StockCheckOutForm();

    }

    public function validate_confirmation() {
        // 1. sprawdzenie, czy parametry zostały przekazane
        // - nie trzeba sprawdzać
        //$this->form->item_id = ParamUtils::getFromCleanURL(1, true, 'Błędne wywołanie aplikacji');
        $this->form_checkout->user_id = ParamUtils::getFromRequest('customer_id', true, 'Błędne wywołanie aplikacji');
        $this->form_checkout->location_id = ParamUtils::getFromRequest('location_id', true, 'Błędne wywołanie aplikacji');
        $this->form_checkout->item_id = ParamUtils::getFromRequest('item_id', true, 'Błędne wywołanie aplikacji');

        return !App::getMessages()->isError();
    }

    public function validate() {
        // 1. sprawdzenie, czy parametry zostały przekazane
        // - nie trzeba sprawdzać
        $this->form->item_id = ParamUtils::getFromCleanURL(1, true, 'Błędne wywołanie aplikacji');
        //ParamUtils::getFromRequest('customer_id');
        return !App::getMessages()->isError();
    }
    public function action_ItemCheckOut() {
        // 1. Walidacja danych formularza (z pobraniem)
        $this->validate();


        //wykonanie zapytania

        try {
            $this->records = App::getDB()->select("items",
                [
                    "[>]locations(locations_default)" => ['item_default_location'=>'location_id'],
                    "[>]models" => ['item_model'=>'model_id'],
                ],
                [
                    "item_name",
                    "item_location",
                    "item_model",
                    "locations_default.location_name(item_default_location_name)",
                    "item_id",
                ],
                [
                    "item_id"=>$this->form->item_id
                ]);


        } catch (\PDOException $e) {
            Utils::addErrorMessage('Wystąpił błąd podczas pobierania rekordów');
            if (App::getConf()->debug)
                Utils::addErrorMessage($e->getMessage());
        }
        try {
            $this->records_users = App::getDB()->select("users", [
                "user_id",
                "name",
                "surname",
                "username",
                "full_name"=>Medoo::raw("CONCAT(`name`, ' ', `surname`)")
            ]);
        } catch (\PDOException $e) {
            Utils::addErrorMessage('Wystąpił błąd podczas pobierania rekordów');
            if (App::getConf()->debug)
                Utils::addErrorMessage($e->getMessage());
        }
        // App::getSmarty()->assign('cust_ids', array(56,92,13));
        App::getSmarty()->assign('cust_ids', array_column($this->records_users, 'user_id'));
        App::getSmarty()->assign('cust_names', array_column($this->records_users, 'full_name'));

        try {
            $this->records_locations = App::getDB()->select("locations", [
                "location_id",
                "location_name",
            ]);
        } catch (\PDOException $e) {
            Utils::addErrorMessage('Wystąpił błąd podczas pobierania rekordów');
            if (App::getConf()->debug)
                Utils::addErrorMessage($e->getMessage());
        }

       // App::getSmarty()->assign('cust_ids', array(56,92,13));
        App::getSmarty()->assign('location_ids', array_column($this->records_locations, 'location_id'));
        App::getSmarty()->assign('location_names', array_column($this->records_locations, 'location_name'));
        // 4. wygeneruj widok
        App::getSmarty()->assign('items', $this->records);  // lista rekordów z bazy danych
        App::getSmarty()->assign('users', $this->records_users);  // lista rekordów z bazy danych
        App::getSmarty()->assign('_SESSION', $_SESSION);
        App::getSmarty()->display('StockCheckOut.tpl');
    }

    public function action_itemCheckOutConfirmation(){
        $this->validate_confirmation();
        try {
            $this->records = App::getDB()->update("items",
                [
                    'user_id'=>$this->form_checkout->user_id,
                    'item_location'=>$this->form_checkout->location_id],
                [
                    "item_id"=>$this->form_checkout->item_id
                ]);


        } catch (\PDOException $e) {
            Utils::addErrorMessage('Wystąpił błąd podczas aktualizowania rekordów');
            if (App::getConf()->debug)
                Utils::addErrorMessage($e->getMessage());
        }
        App::getSmarty()->assign('_SESSION', $_SESSION);
        App::getRouter()->forwardTo("itemList");
    }

}
