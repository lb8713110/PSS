<?php

namespace app\controllers;

use core\App;
use core\Utils;
use core\ParamUtils;
use app\forms\LocationSearchForm;

class LocationEditCtrl {

    private $form; //dane formularza wyszukiwania
    private $records; //rekordy pobrane z bazy danych

    public function __construct() {
        //stworzenie potrzebnych obiektów
        $this->form = new LocationSearchForm();
    }

    public function validateEdit() {
        // 1. sprawdzenie, czy parametry zostały przekazane
        // - nie trzeba sprawdzać
        $this->form->location_id = ParamUtils::getFromCleanURL(1, true, 'Błędne wywołanie aplikacji');

        // 2. sprawdzenie poprawności przekazanych parametrów
        // - nie trzeba sprawdzać

        return !App::getMessages()->isError();
    }
    public function action_locationEdit() {
        // 1. walidacja id lokacji do edycji
        if ($this->validateEdit()) {
            try {
                // 2. odczyt z bazy danych osoby o podanym ID (tylko jednego rekordu)
                $record = App::getDB()->get("locations", "*", [
                    "location_id" => $this->form->location_id
                ]);
                // 2.1 jeśli osoba istnieje to wpisz dane do obiektu formularza
                $this->form->location_id = $record['location_id'];
                $this->form->location_name = $record['location_name'];

            } catch (\PDOException $e) {
                Utils::addErrorMessage('Wystąpił błąd podczas odczytu rekordu');
                if (App::getConf()->debug)
                    Utils::addErrorMessage($e->getMessage());
            }
        }

        // 3. Wygenerowanie widoku
        $this->generateView();
    }
    public function validateEditConfirmation()
    {
        $this->form->location_name = ParamUtils::getFromRequest('location_name');
        $this->form->location_id = ParamUtils::getFromRequest('location_id');


        if (empty($this->form->location_name))
            Utils::addErrorMessage('Nie podano Nazwy');
        // sprawdzenie, czy potrzebne wartości zostały przekazane
        return !App::getMessages()->isError();
    }
    public function action_locationSave()
    {
        if ($this->validateEditConfirmation()) {
            try {
                App::getDB()->update("locations", [
                    "location_name" => $this->form->location_name,

                ], [
                    "location_id" => $this->form->location_id
                ]);
            }catch (\PDOException $e) {
                Utils::addErrorMessage('Wystąpił nieoczekiwany błąd podczas zapisu rekordu');
                if (App::getConf()->debug)
                    Utils::addErrorMessage($e->getMessage());
            }

        }
        App::getRouter()->forwardTo('locationList');

    }
    public function action_locationDelete() {
        // 1. walidacja id osoby do usuniecia
        if ($this->validateEdit()) {

            try {
                // 2. usunięcie rekordu
                App::getDB()->delete("locations", [
                    "location_id" => $this->form->location_id
                ]);
                Utils::addInfoMessage('Pomyślnie usunięto rekord');
            } catch (\PDOException $e) {
                Utils::addErrorMessage('Wystąpił błąd podczas usuwania rekordu');
                if (App::getConf()->debug)
                    Utils::addErrorMessage($e->getMessage());
            }
        }

        // 3. Przekierowanie na stronę listy przedmiotow
        App::getRouter()->forwardTo('locationList');
    }
    public function generateView() {

        App::getSmarty()->assign('location_name', $this->form->location_name);
        App::getSmarty()->assign('location_id', $this->form->location_id);

        App::getSmarty()->assign('_SESSION', $_SESSION);

        App::getSmarty()->display('LocationEdit.tpl');
    }


}
