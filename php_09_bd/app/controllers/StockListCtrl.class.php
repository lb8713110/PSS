<?php

namespace app\controllers;

use core\App;
use core\Utils;
use core\ParamUtils;
use app\forms\StockSearchForm;

class StockListCtrl {

    private $form; //dane formularza wyszukiwania
    private $records; //rekordy pobrane z bazy danych

    public function __construct() {
        //stworzenie potrzebnych obiektów
        $this->form = new StockSearchForm();
    }

    public function validate() {
        // 1. sprawdzenie, czy parametry zostały przekazane
        // - nie trzeba sprawdzać
        $this->form->item_id = ParamUtils::getFromRequest('sf_name');

        // 2. sprawdzenie poprawności przekazanych parametrów
        // - nie trzeba sprawdzać

        return !App::getMessages()->isError();
    }

    public function action_ItemList() {
        // 1. Walidacja danych formularza (z pobraniem)
        $this->validate();

        // 2. Przygotowanie mapy z parametrami wyszukiwania (nazwa_kolumny => wartość)
        $search_params = []; //przygotowanie pustej struktury (aby była dostępna nawet gdy nie będzie zawierała wierszy)
        if (isset($this->form->item_id) && strlen($this->form->item_id) > 0) {
            $search_params['item_name[~]'] = $this->form->item_id . '%'; // dodanie symbolu % zastępuje dowolny ciąg znaków na końcu
        }

        // 3. Pobranie listy rekordów z bazy danych
        // W tym wypadku zawsze wyświetlamy listę osób bez względu na to, czy dane wprowadzone w formularzu wyszukiwania są poprawne.
        // Dlatego pobranie nie jest uwarunkowane poprawnością walidacji (jak miało to miejsce w kalkulatorze)
        //przygotowanie frazy where na wypadek większej liczby parametrów
        $num_params = sizeof($search_params);
        if ($num_params > 1) {
            $where = ["AND" => &$search_params];
        } else {
            $where = &$search_params;
        }
        //dodanie frazy sortującej po nazwisku
        $where ["ORDER"] = "item_name";
        //wykonanie zapytania

        try {
            $this->records = App::getDB()->select("items", [

            //joiny  "[>]locations" => ['item_location'=>'location_id'],
                "[>]locations(locations_default)" => ['item_default_location'=>'location_id'],
                "[>]locations(locations_current)" => ['item_location'=>'location_id'],

                "[>]models" => ['item_model'=>'model_id'],
                "[>]users" => ['user_id'=>'user_id'],
                ],
                //columns
                //
                [

                "item_name",
                "locations_current.location_name(item_location_name)",
                "models.name(item_model_name)",
                "locations_default.location_name(item_default_location_name)",
                "item_id",
                "users.surname(user_id_surname)",
                    "users.user_id"
            ], $where);


        } catch (\PDOException $e) {
            Utils::addErrorMessage('Wystąpił błąd podczas pobierania rekordów');
            if (App::getConf()->debug)
                Utils::addErrorMessage($e->getMessage());
        }

        // 4. wygeneruj widok
        App::getSmarty()->assign('searchForm', $this->form); // dane formularza (wyszukiwania w tym wypadku)
        App::getSmarty()->assign('items', $this->records);  // lista rekordów z bazy danych
        App::getSmarty()->assign('_SESSION', $_SESSION);

        App::getSmarty()->display('StockList.tpl');
    }

}
