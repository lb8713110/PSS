<?php

namespace app\controllers;

use core\App;
use core\Utils;
use core\ParamUtils;
use core\Validator;
use app\forms\StockItemNewForm;

class StockItemNewCtrl {

    private $form; //dane formularza

    public function __construct() {
        //stworzenie potrzebnych obiektów
        $this->form = new StockItemNewForm();
        $this->roles_names = [];
        $this->roles_ids = [];
        $this->user_roles = [];

    }

    // Walidacja danych przed zapisem (nowe dane lub edycja).
    public function validateSave() {
        //0. Pobranie parametrów z walidacją
        $this->form->item_name = ParamUtils::getFromRequest('name', true, 'Błędne wywołanie aplikacji');
        $this->form->item_default_location = ParamUtils::getFromRequest('location_id', true, 'Błędne wywołanie aplikacji');
        $this->form->item_model = ParamUtils::getFromRequest('model_id', true, 'Błędne wywołanie aplikacji');

        if (App::getMessages()->isError())
            return false;

        // 1. sprawdzenie czy wartości wymagane nie są puste
        if (empty(trim($this->form->item_name))) {
            Utils::addErrorMessage('Wprowadź nazwę towaru');
        }

        if (App::getMessages()->isError())
            return false;

        // 2. sprawdzenie poprawności przekazanych parametrów



        return !App::getMessages()->isError();
    }

    //validacja danych przed wyswietleniem do edycji


    public function action_stockItemNew() {
        $this->generateView();
    }

    //wysiweltenie rekordu do edycji wskazanego parametrem 'id'


    public function getUserIdFromUsername($username)
    {
        try {
            return App::getDB()->select('users', ['user_id'],
                ['username' => $this->form->username])[0];


        } catch (\PDOException $e) {
            Utils::addErrorMessage('Wystąpił błąd podczas pobierania rekordów');
            if (App::getConf()->debug)
                Utils::addErrorMessage($e->getMessage());
        }
        return false;
    }

    public function action_stockItemNewConfirm() {

        // 1. Walidacja danych formularza (z pobraniem)
        if ($this->validateSave()) {
            // 2. Zapis danych w bazie
            try {

                //2.1 Nowy rekord
                        App::getDB()->insert("items", [
                            "item_name" => $this->form->item_name,
                            "item_location" => $this->form->item_default_location,
                            "item_default_location" => $this->form->item_default_location,
                            "item_model" => $this->form->item_model
                        ]);
                        Utils::addInfoMessage('Pomyślnie zapisano rekord');
                    }
                            catch (\PDOException $e) {
                                Utils::addErrorMessage('Wystąpił błąd podczas usuwania rekordu');
                                if (App::getConf()->debug)
                                    Utils::addErrorMessage($e->getMessage());
                            }

                        }

                        // 3b. Po zapisie przejdź na stronę listy osób (w ramach tego samego żądania http)
            App::getRouter()->forwardTo('itemList');
        }



    public function generateView() {

        try {
            $this->records_locations = App::getDB()->select("locations", [
                "location_id",
                "location_name",
            ]);
        } catch (\PDOException $e) {
            Utils::addErrorMessage('Wystąpił błąd podczas pobierania rekordów');
            if (App::getConf()->debug)
                Utils::addErrorMessage($e->getMessage());
        }
        try {
            $this->records_models = App::getDB()->select("models", [
                "model_id",
                "name",
            ]);
        } catch (\PDOException $e) {
            Utils::addErrorMessage('Wystąpił błąd podczas pobierania rekordów');
            if (App::getConf()->debug)
                Utils::addErrorMessage($e->getMessage());
        }
        App::getSmarty()->assign('model_ids', array_column($this->records_models, 'model_id'));
        App::getSmarty()->assign('model_names', array_column($this->records_models, 'name'));
        App::getSmarty()->assign('location_ids', array_column($this->records_locations, 'location_id'));
        App::getSmarty()->assign('location_names', array_column($this->records_locations, 'location_name'));
        App::getSmarty()->assign('form', $this->form); // dane formularza dla widoku
        App::getSmarty()->assign('_SESSION', $_SESSION);

        App::getSmarty()->display('StockItemNew.tpl');
    }

}
