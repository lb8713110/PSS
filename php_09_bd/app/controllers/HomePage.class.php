<?php

namespace app\controllers;

use app\forms\HomePageForm;
use core\App;
use core\Utils;
use core\RoleUtils;
use core\ParamUtils;
use app\forms\LoginForm;

class homePage {

    private $form;

    public function __construct() {
        //stworzenie potrzebnych obiektów
        $this->form = new HomePageForm();
    }


    public function action_homePage() {
        $this->generateView();
    }

    public function generateView() {
        App::getSmarty()->assign('form', $this->form); // dane formularza do widoku
        App::getSmarty()->assign('_SESSION', $_SESSION);
        App::getSmarty()->display('homePage.tpl');
    }

}
