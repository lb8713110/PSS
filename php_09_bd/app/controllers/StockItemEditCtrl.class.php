<?php

namespace app\controllers;

use core\App;
use core\Utils;
use core\ParamUtils;
use app\forms\StockEditForm;

class StockItemEditCtrl {

    private $form; //dane formularza wyszukiwania
    private $records; //rekordy pobrane z bazy danych

    public function __construct() {
        //stworzenie potrzebnych obiektów
        $this->form = new StockEditForm();
    }

    public function validate() {
        // 1. sprawdzenie, czy parametry zostały przekazane
        // - nie trzeba sprawdzać
        $this->form->item_id = ParamUtils::getFromRequest('item_id');

        // 2. sprawdzenie poprawności przekazanych parametrów
        // - nie trzeba sprawdzać

        return !App::getMessages()->isError();
    }
    public function validateEdit() {
        //pobierz parametry na potrzeby wyswietlenia danych do edycji
        //z widoku listy itemów (parametr jest wymagany)
        $this->form->item_id = ParamUtils::getFromCleanURL(1, true, 'Błędne wywołanie aplikacji');
        return !App::getMessages()->isError();
    }
public function validateEditConfirmation()
{
    $this->form->item_name = ParamUtils::getFromRequest('item_name');
    $this->form->item_id = ParamUtils::getFromRequest('item_id');
    $this->form->item_model_id = ParamUtils::getFromRequest('model_id');
    $this->form->item_default_location_id = ParamUtils::getFromRequest('location_id');

    if (empty($this->form->item_name))
        Utils::addErrorMessage('Nie podano Nazwy');
    // sprawdzenie, czy potrzebne wartości zostały przekazane
    if (empty($this->form->item_model_id)) {
        Utils::addErrorMessage('Nie podano modelu');
    }
    if (empty($this->form->item_default_location_id)) {
        Utils::addErrorMessage('Nie podano lokacji');
    }
    return !App::getMessages()->isError();
}
public function action_ItemDelete() {
    // 1. walidacja id osoby do usuniecia
    if ($this->validateEdit()) {

        try {
            // 2. usunięcie rekordu
            App::getDB()->delete("items", [
                "item_id" => $this->form->item_id
            ]);
            Utils::addInfoMessage('Pomyślnie usunięto rekord');
        } catch (\PDOException $e) {
            Utils::addErrorMessage('Wystąpił błąd podczas usuwania rekordu');
            if (App::getConf()->debug)
                Utils::addErrorMessage($e->getMessage());
        }
    }

    // 3. Przekierowanie na stronę listy przedmiotow
    App::getRouter()->forwardTo('itemList');
}
    public function action_ItemEdit() {
        // 1. walidacja id osoby do edycji
        if ($this->validateEdit()) {
            try {
                $this->records = App::getDB()->select("items", [

                    //joiny  "[>]locations" => ['item_location'=>'location_id'],
                    "[>]locations(locations_default)" => ['item_default_location'=>'location_id'],
                    "[>]locations(locations_current)" => ['item_location'=>'location_id'],

                    "[>]models" => ['item_model'=>'model_id'],
                    "[>]users" => ['user_id'=>'user_id'],
                ],
                    //columns
                    //
                    [

                        "item_name",
                        "locations_current.location_name(item_location_name)",
                        "models.name(item_model_name)",
                        "locations_default.location_name(item_default_location_name)",
                        "item_id",
                        "users.surname(user_id_surname)",
                        "users.user_id",
                        "items.item_location(item_location_id)",
                        "items.item_default_location(item_default_location_id)",
                        "items.item_model(item_selected_model_id)"

                    ],['item_id' => intval($this->form->item_id)]);


                // 2. odczyt z bazy danych itemu o podanym ID (tylko jednego rekordu)


                // 2.1 jeśli osoba istnieje to wpisz dane do obiektu formularza
                $this->form->item_name = $this->records[0]['item_name'];
                $this->form->item_location_name = $this->records[0]['item_location_name'];
                $this->form->item_model_name = $this->records[0]['item_model_name'];
                $this->form->item_default_location_name = $this->records[0]['item_default_location_name'];
                $this->form->item_id = $this->records[0]['item_id'];
                $this->form->user_id_surname = $this->records[0]['user_id_surname'];
                $this->form->user_id = $this->records[0]['user_id'];
                $this->form->item_selected_default_location_id = $this->records[0]['item_default_location_id'];
                $this->form->item_selected_model_id = $this->records[0]['item_selected_model_id'];


            } catch (\PDOException $e) {
                Utils::addErrorMessage('Wystąpił błąd podczas odczytu rekordu');
                if (App::getConf()->debug)
                    Utils::addErrorMessage($e->getMessage());
            }
        }

        // 3. Wygenerowanie widoku
        $this->generateView();
    }
    public function action_ItemEditConfirmation(){
        if ($this->validateEditConfirmation()) {
            try {
                App::getDB()->update("items", [
                    "item_name" => $this->form->item_name,
                    "item_default_location" => $this->form->item_default_location_id,
                    "item_model" => $this->form->item_model_id
                ], [
                    "item_id" => $this->form->item_id
                ]);
            }catch (\PDOException $e) {
                Utils::addErrorMessage('Wystąpił nieoczekiwany błąd podczas zapisu rekordu');
                if (App::getConf()->debug)
                    Utils::addErrorMessage($e->getMessage());
            }

            }
        App::getRouter()->forwardTo('itemList');
    }

    public function generateView() {

        try {
            $this->records_locations = App::getDB()->select("locations", [
                "location_id",
                "location_name",
            ]);
        } catch (\PDOException $e) {
            Utils::addErrorMessage('Wystąpił błąd podczas pobierania rekordów');
            if (App::getConf()->debug)
                Utils::addErrorMessage($e->getMessage());
        }
        try {
            $this->records_models = App::getDB()->select("models", [
                "model_id",
                "name",
            ]);
        } catch (\PDOException $e) {
            Utils::addErrorMessage('Wystąpił błąd podczas pobierania rekordów');
            if (App::getConf()->debug)
                Utils::addErrorMessage($e->getMessage());
        }
        App::getSmarty()->assign('model_ids', array_column($this->records_models, 'model_id'));
        App::getSmarty()->assign('model_names', array_column($this->records_models, 'name'));
        App::getSmarty()->assign('location_ids', array_column($this->records_locations, 'location_id'));
        App::getSmarty()->assign('location_names', array_column($this->records_locations, 'location_name'));
        App::getSmarty()->assign('selected_location_id', $this->form->item_selected_default_location_id);
        App::getSmarty()->assign('selected_model_id', $this->form->item_selected_model_id);


        App::getSmarty()->assign('items', $this->records); // dane formularza dla widoku
        App::getSmarty()->assign('_SESSION', $_SESSION);

        App::getSmarty()->display('StockEdit.tpl');
    }
}
