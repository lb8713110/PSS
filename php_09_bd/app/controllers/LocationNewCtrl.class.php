<?php

namespace app\controllers;

use core\App;
use core\Utils;
use core\ParamUtils;
use app\forms\LocationSearchForm;

class LocationNewCtrl
{

    private $form; //dane formularza wyszukiwania
    private $records; //rekordy pobrane z bazy danych

    public function __construct()
    {
        //stworzenie potrzebnych obiektów
        $this->form = new LocationSearchForm();
    }

    public function validateSave()
    {
        // Pobranie parametrów z formularza
        $this->form->name = ParamUtils::getFromRequest('name');

        // Walidacja danych
        if (empty($this->form->name)) {
            Utils::addErrorMessage('Wszystkie pola są wymagane');
            return false;
        }

        return !App::getMessages()->isError();
    }

    public function validate()
    {
        // 1. sprawdzenie, czy parametry zostały przekazane
        // - nie trzeba sprawdzać
        $this->form->location_name = ParamUtils::getFromRequest('sf_name');

        // 2. sprawdzenie poprawności przekazanych parametrów
        // - nie trzeba sprawdzać

        return !App::getMessages()->isError();
    }

    public function action_LocationNew()
    {

        // 1. Walidacja danych formularza (z pobraniem)
        if ($this->validateSave()) {
            // 2. Zapis danych w bazie
            try {
                // Rozpoczęcie transakcji
                App::getDB()->pdo->beginTransaction();

                // Wstawienie danych użytkownika do tabeli 'users'
                App::getDB()->insert("locations", [
                    "location_name" => $this->form->name
                ]);
                // Zatwierdzenie transakcji
                App::getDB()->pdo->commit();

            } catch (\PDOException $e) {
                // W przypadku błędu transakcji, cofnięcie zmian
                App::getDB()->pdo->rollBack();

                Utils::addErrorMessage('Wystąpił nieoczekiwany błąd podczas zapisu rekordu');
                if (App::getConf()->debug)
                    Utils::addErrorMessage($e->getMessage());
            }

            // Przekierowanie na inną stronę po zapisaniu danych
            App::getRouter()->forwardTo('locationList');
        } else {
            // Pozostanie na stronie w przypadku błędów walidacji
            $this->generateView();
        }
    }

    public function generateView()
    {
        // Przekazanie danych formularza do widoku
        App::getSmarty()->assign('form', $this->form);
        App::getSmarty()->assign('_SESSION', $_SESSION);
        // Wyświetlenie widoku
        App::getSmarty()->display('LocationNewView.tpl');
    }
}