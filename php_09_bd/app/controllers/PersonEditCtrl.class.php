<?php

namespace app\controllers;

use core\App;
use core\Utils;
use core\ParamUtils;
use core\Validator;
use app\forms\PersonEditForm;

class PersonEditCtrl {

    private $form; //dane formularza

    public function __construct() {
        //stworzenie potrzebnych obiektów
        $this->form = new PersonEditForm();
        $this->roles_names = [];
        $this->roles_ids = [];
        $this->user_roles = [];

    }

    // Walidacja danych przed zapisem (nowe dane lub edycja).
    public function validateSave() {
        //0. Pobranie parametrów z walidacją
        $this->form->username = ParamUtils::getFromRequest('username', true, 'Błędne wywołanie aplikacji');
        $this->form->name = ParamUtils::getFromRequest('name', true, 'Błędne wywołanie aplikacji');
        $this->form->surname = ParamUtils::getFromRequest('surname', true, 'Błędne wywołanie aplikacji');
        $this->form->role_checkboxes = ParamUtils::getFromRequest('roles');
//        $password1 = ParamUtils::getFromRequest('password');
//        $password2 = ParamUtils::getFromRequest('password2');
//        if ($password2 != $password1) {
//            Utils::addErrorMessage('Powtórz poprawnie hasło');
//            return false;
//        }
//        $this->form->password = $password1;


        // 1. sprawdzenie czy wartości wymagane nie są puste
        if (empty(trim($this->form->name))) {
            Utils::addErrorMessage('Wprowadź imię');
        }
        if (empty(trim($this->form->surname))) {
            Utils::addErrorMessage('Wprowadź nazwisko');
        }
        if (empty(trim($this->form->username))) {
            Utils::addErrorMessage('Wprowadż nazwę użytkownika');
        }

        if (App::getMessages()->isError())
            return false;

        // 2. sprawdzenie poprawności przekazanych parametrów



        return !App::getMessages()->isError();
    }

    //validacja danych przed wyswietleniem do edycji
    public function validateEdit() {
        //pobierz parametry na potrzeby wyswietlenia danych do edycji
        //z widoku listy osób (parametr jest wymagany)
        $this->form->username = ParamUtils::getFromCleanURL(1, true, 'Błędne wywołanie aplikacji');
        return !App::getMessages()->isError();
    }

    public function action_personNew() {
        $this->generateView();
    }

    //wysiweltenie rekordu do edycji wskazanego parametrem 'id'
    public function action_personEdit() {
        // 1. walidacja id osoby do edycji
        if ($this->validateEdit()) {
            try {
                // 2. odczyt z bazy danych osoby o podanym ID (tylko jednego rekordu)
                $record = App::getDB()->get("users", "*", [
                    "username" => $this->form->username
                ]);
                // 2.1 jeśli osoba istnieje to wpisz dane do obiektu formularza
                $this->form->username = $record['username'];
                $this->form->name = $record['name'];
                $this->form->surname = $record['surname'];

            } catch (\PDOException $e) {
                Utils::addErrorMessage('Wystąpił błąd podczas odczytu rekordu');
                if (App::getConf()->debug)
                    Utils::addErrorMessage($e->getMessage());
            }
        }

        // 3. Wygenerowanie widoku
        $this->generateView();
    }

    public function action_personDelete() {
        // 1. walidacja id osoby do usuniecia
        if ($this->validateEdit()) {

            try {
                // 2. usunięcie rekordu
                App::getDB()->delete("users", [
                    "username" => $this->form->username
                ]);
                Utils::addInfoMessage('Pomyślnie usunięto rekord');
            } catch (\PDOException $e) {
                Utils::addErrorMessage('Wystąpił błąd podczas usuwania rekordu');
                if (App::getConf()->debug)
                    Utils::addErrorMessage($e->getMessage());
            }
        }

        // 3. Przekierowanie na stronę listy osób
        App::getRouter()->forwardTo('personList');
    }

    public function set_roles_for_user(){
;
    }

    public function getUserIdFromUsername($username)
    {
        try {
            return App::getDB()->select('users', ['user_id'],
                ['username' => $this->form->username])[0];


        } catch (\PDOException $e) {
            Utils::addErrorMessage('Wystąpił błąd podczas pobierania rekordów');
            if (App::getConf()->debug)
                Utils::addErrorMessage($e->getMessage());
        }
        return false;
    }

    public function action_personSave() {

        // 1. Walidacja danych formularza (z pobraniem)
        if ($this->validateSave()) {
            // 2. Zapis danych w bazie
            try {

                //2.1 Nowy rekord
                if ($this->form->username == '') {
                    //sprawdź liczebność rekordów - nie pozwalaj przekroczyć 20
                    $count = App::getDB()->count("users");
                    if ($count <= 20) {
                        App::getDB()->insert("users", [
                            "name" => $this->form->name,
                            "surname" => $this->form->surname,
                            "username" => $this->form->username,
                            "password" => $this->form->password
                        ]);
                    } else { //za dużo rekordów
                        // Gdy za dużo rekordów to pozostań na stronie
                        Utils::addInfoMessage('Ograniczenie: Zbyt dużo rekordów. Aby dodać nowy usuń wybrany wpis.');
                        $this->generateView(); //pozostań na stronie edycji
                        exit(); //zakończ przetwarzanie, aby nie dodać wiadomości o pomyślnym zapisie danych
                    }
                } else {
                    //2.2 Edycja rekordu o danym ID
                    App::getDB()->update("users", [
                        "name" => $this->form->name,
                        "surname" => $this->form->surname,
                        "username" => $this->form->username
                    ], [
                        "username" => $this->form->username
                    ]);
                    /////role
                    ///
                    //$user_roles
                    $this->getRoleForUserFromDB();


                    //usun nadmiarowe role:
                    foreach ($this->user_roles as $role)
                    {
                        if (!empty($this->form->role_checkboxes)){
                        if(!in_array($role['role_id'], $this->form->role_checkboxes))
                        {
                            //Jesli roli z bazy danych nie ma w obecnych checkboxach - trzeba ja usunac!
                            $del_role = $role['role_id'];
                            try {
                                // 2. usunięcie rekordu
                                App::getDB()->delete("users_roles", ['AND' => [
                                    "users_user_id" => $this->getUserIdFromUsername($this->form->username),
                                    "roles_role_id" => $del_role]
                                ]);
                                Utils::addInfoMessage('Pomyślnie usunięto rekord');
                            } catch (\PDOException $e) {
                                Utils::addErrorMessage('Wystąpił błąd podczas usuwania rekordu');
                                if (App::getConf()->debug)
                                    Utils::addErrorMessage($e->getMessage());
                            }
                      }
                        }
                        else
                        {
                            //zadnej roli nie zaznaczono - oznacz usun wszysktie
                            try {
                                // 2. usunięcie rekordu
                                App::getDB()->delete("users_roles", [
                                    "users_user_id" => $this->getUserIdFromUsername($this->form->username)
                                ]);
                                Utils::addInfoMessage('Pomyślnie usunięto rekord');
                            } catch (\PDOException $e) {
                                Utils::addErrorMessage('Wystąpił błąd podczas usuwania rekordu');
                                if (App::getConf()->debug)
                                    Utils::addErrorMessage($e->getMessage());
                            }

                        }
                    }


                    //dodaj role
                    if (!empty($this->form->role_checkboxes)){
                    foreach ($this->form->role_checkboxes as $role)
                    {

                    if (!in_array($role, array_column($this->user_roles, 'role_id')))
                    {
                        // brakuje roli, dodajmy
                        $add_role = $role;
                        $user_id = $this->getUserIdFromUsername($this->form->username);
                        $add_role = intval($add_role);
                        $user_id = intval($user_id['user_id']);
                        App::getDB()->insert("users_roles", [

                            "users_user_id" => $user_id,
                            "roles_role_id" => $add_role
                        ]);

                    }

                    }}
                }
                Utils::addInfoMessage('Pomyślnie zapisano rekord');
            } catch (\PDOException $e) {
                Utils::addErrorMessage('Wystąpił nieoczekiwany błąd podczas zapisu rekordu');
                if (App::getConf()->debug)
                    Utils::addErrorMessage($e->getMessage());
            }

            // 3b. Po zapisie przejdź na stronę listy osób (w ramach tego samego żądania http)
            App::getRouter()->forwardTo('personList');
        } else {
            // 3c. Gdy błąd walidacji to pozostań na stronie
            $this->generateView();
        }
    }
    public function getRolesFromDB()
    {
        $records = [];
        try {
            $records = App::getDB()->select("roles", [
                "role_id",
                "type"
            ]);
        } catch (\PDOException $e) {
            Utils::addErrorMessage('Wystąpił błąd podczas pobierania rekordów');
            if (App::getConf()->debug)
                Utils::addErrorMessage($e->getMessage());
        }
        $this->roles_names = array_column($records, 'type');
        $this->roles_ids = array_column($records, 'role_id');
    }
    public function getRoleForUserFromDB()
    {
        $records = [];
        try {
            $this->user_roles = App::getDB()->select('users', [
                '[>]users_roles' => ['user_id' => 'users_user_id'],
                '[>]roles' => ['users_roles.roles_role_id' => 'role_id']
            ], [
                'roles.role_id'
            ], [
                'username' => $this->form->username
            ]);


        } catch (\PDOException $e) {
            Utils::addErrorMessage('Wystąpił błąd podczas pobierania rekordów');
            if (App::getConf()->debug)
                Utils::addErrorMessage($e->getMessage());
        }
    }

    public function generateView() {
        $this->getRolesFromDB();
        $this->getRoleForUserFromDB();
        $user_roles = array_column($this->user_roles, 'role_id');
        App::getSmarty()->assign('roles_names', $this->roles_names);
        App::getSmarty()->assign('default_role_id', $user_roles);
        App::getSmarty()->assign('roles_ids', $this->roles_ids);
        App::getSmarty()->assign('form', $this->form); // dane formularza dla widoku
        App::getSmarty()->assign('_SESSION', $_SESSION);

        App::getSmarty()->display('PersonEdit.tpl');
    }

}
