<?php

namespace app\controllers;

use core\App;
use core\Utils;
use core\RoleUtils;
use core\ParamUtils;
use app\forms\LoginForm;

class LoginCtrl {

    private $form;

    public function __construct() {
        //stworzenie potrzebnych obiektów
        $this->form = new LoginForm();
    }

    public function validate() {
        $this->form->login = ParamUtils::getFromRequest('login');
        $this->form->pass = ParamUtils::getFromRequest('pass');


        //nie ma sensu walidować dalej, gdy brak parametrów
        if (!isset($this->form->login))
            return false;
        // sprawdzenie, czy potrzebne wartości zostały przekazane
        if (empty($this->form->login)) {
            Utils::addErrorMessage('Nie podano loginu');
        }
        if (empty($this->form->pass)) {
            Utils::addErrorMessage('Nie podano hasła');
        }
        //nie ma sensu walidować dalej, gdy brak wartości
        if (App::getMessages()->isError())
            return false;


        $search_params = []; //przygotowanie pustej struktury (aby była dostępna nawet gdy nie będzie zawierała wierszy)

        $search_params['username'] = $this->form->login;
        $num_params = sizeof($search_params);
        if ($num_params > 1) {
            $where = ["AND" => &$search_params];
        } else {
            $where = &$search_params;
        }
        try {
            $this->form->records = App::getDB()->select("users", [
                "username",
                "name",
                "surname",
                "password",
                "user_id"
            ], $where);
        } catch (\PDOException $e) {
            Utils::addErrorMessage('Wystąpił błąd podczas pobierania rekordów');
            if (App::getConf()->debug)
                Utils::addErrorMessage($e->getMessage());
        }
        $user_exists=!empty($this->form->records);
        if ($user_exists && $this->form->login == $this->form->records[0]["username"] && $this->form->pass == $this->form->records[0]["password"]) {



//////////////Role
            $search_params = []; //przygotowanie pustej struktury (aby była dostępna nawet gdy nie będzie zawierała wierszy)

            $search_params['users_user_id'] = $this->form->records[0]["user_id"];
            $num_params = sizeof($search_params);
            if ($num_params > 1) {
                $where = ["AND" => &$search_params];
            } else {
                $where = &$search_params;
            }
            try {
                $roles_associated_table = App::getDB()->select("users_roles", [
                    "roles_role_id"
                ], $where);
            } catch (\PDOException $e) {
                Utils::addErrorMessage('Wystąpił błąd podczas pobierania rekordów');
                if (App::getConf()->debug)
                    Utils::addErrorMessage($e->getMessage());
            }

/////////////
/// dekodowanie typow ról
            $_SESSION['admin']=NULL;
            foreach ($roles_associated_table as $rat)
            {

            $search_params = []; //przygotowanie pustej struktury (aby była dostępna nawet gdy nie będzie zawierała wierszy)

            $search_params['role_id'] = $rat["roles_role_id"];
            $num_params = sizeof($search_params);
            if ($num_params > 1) {
                $where = ["AND" => &$search_params];
            } else {
                $where = &$search_params;
            }

            try {
                $roles_table = App::getDB()->select("roles", [
                    "role_id",
                    "type"
                ], $where);

                RoleUtils::addRole($roles_table[0]["type"]);
                if ($_SESSION['admin'] == true || $roles_table[0]["type"] == "admin")
                {
                    $_SESSION['admin'] = true;
                }
                else
                {
                    $_SESSION['admin'] = false;
                }


            } catch (\PDOException $e) {
                Utils::addErrorMessage('Wystąpił błąd podczas pobierania rekordów');
                if (App::getConf()->debug)
                    Utils::addErrorMessage($e->getMessage());
            }



        }//foreach

        } else {
            Utils::addErrorMessage('Niepoprawny login lub hasło');
        }
        return !App::getMessages()->isError();
    }

    public function action_loginShow() {
        $this->generateView();
    }

    public function action_login() {
        if ($this->validate()) {
            //zalogowany => przekieruj na główną akcję (z przekazaniem messages przez sesję)
            Utils::addInfoMessage('Poprawnie zalogowano do systemu '. $this->form->records[0]["name"] . " ".
                                                                            $this->form->records[0]["surname"]);
            App::getSmarty()->assign('form', $this->form); // dane formularza do widoku
            //$this->generateView();
            App::getRouter()->redirectTo("homePage");
        } else {
            //niezalogowany => pozostań na stronie logowania
            $this->generateView();
        }
    }

    public function action_logout() {
        // 1. zakończenie sesji
        session_destroy();
        // 2. idź na stronę główną - system automatycznie przekieruje do strony logowania
        App::getRouter()->redirectTo('homePage');
    }

    public function generateView() {
        App::getSmarty()->assign('_SESSION', $_SESSION);
        App::getSmarty()->assign('form', $this->form); // dane formularza do widoku
        App::getSmarty()->display('LoginView.tpl');
    }

}
