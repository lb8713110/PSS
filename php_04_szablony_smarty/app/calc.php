<?php
// KONTROLER strony kalkulatora
require_once dirname(__FILE__).'/../config.php';
//załaduj Smarty
require_once _ROOT_PATH.'/lib/smarty/Smarty.class.php';

//pobranie parametrów
function getParams(&$form){
    $form['how_many_years'] = isset($_REQUEST['how_many_years']) ? $_REQUEST['how_many_years'] : null;
    $form['amount'] = isset($_REQUEST['amount']) ? $_REQUEST['amount'] : null;
    $form['percentage'] = isset($_REQUEST['percentage']) ? $_REQUEST['percentage'] : null;
}

//walidacja parametrów z przygotowaniem zmiennych dla widoku
function validate(&$form,&$infos,&$messages,&$hide_intro){

    if ( ! (isset($form['how_many_years']) && isset($form['amount']) && isset($form['percentage']))) {
        return false;
    }
    $hide_intro = true;

    // sprawdzenie, czy potrzebne wartości zostały przekazane
    if ( $form['how_many_years'] == "") {
        $messages [] = 'Nie podano lat';
    }
    if ( $form['amount'] == "") {
        $messages [] = 'Nie podano kwoty';
    }
    if ( $form['percentage'] == "") {
        $messages [] = 'Nie podano oprocentowania';
    }
    //nie ma sensu walidować dalej gdy brak parametrów
    if (count ( $messages ) != 0) return false;

    // sprawdzenie, czy zmienne są liczbami
    if (! is_numeric( $form['how_many_years'] )) {
        $messages [] = 'Lata nie są liczbą!';
    }

    if (! is_numeric( $form['amount'] )) {
        $messages [] = 'Kwota nie jest liczbą!';
    }

    if (! is_numeric( $form['percentage'] )) {
        $messages [] = 'Kwota nie jest liczbą!';
    }

    if (count ( $messages ) != 0) return false;
    else return true;
}

	
// wykonaj obliczenia
function process(&$form,&$infos,&$messages,&$result){
	$infos [] = 'Parametry poprawne. Wykonuję obliczenia.';
    $form['how_many_years'] = intval($form['how_many_years']);
    $form['amount'] = floatval($form['amount']);
    $form['percentage'] = floatval($form['percentage']);

    //wykonanie operacji
    $result = ($form['amount'] * (100+$form['percentage'])/100) / ($form['how_many_years']*12);
}

//inicjacja zmiennych
$form = null;
$infos = array();
$messages = array();
$result = null;
$hide_intro = false;
	
getParams($form);
if ( validate($form,$infos,$messages,$hide_intro) ){
	process($form,$infos,$messages,$result);
}

// 4. Przygotowanie danych dla szablonu

$smarty = new Smarty();

$smarty->assign('app_url',_APP_URL);
$smarty->assign('root_path',_ROOT_PATH);
$smarty->assign('page_title','Kalkulator kredytowy 04');
$smarty->assign('page_description','Na bibliotece Smarty');
$smarty->assign('page_header','Twoja ratka!');

$smarty->assign('hide_intro',$hide_intro);

//pozostałe zmienne niekoniecznie muszą istnieć, dlatego sprawdzamy aby nie otrzymać ostrzeżenia
$smarty->assign('form',$form);
$smarty->assign('result',$result);
$smarty->assign('messages',$messages);
$smarty->assign('infos',$infos);

// 5. Wywołanie szablonu
$smarty->display(_ROOT_PATH.'/app/calc.tpl');