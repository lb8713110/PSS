<?php
require_once dirname(__FILE__).'/../config.php';

// KONTROLER strony kalkulatora

// W kontrolerze niczego nie wysyła się do klienta.
// Wysłaniem odpowiedzi zajmie się odpowiedni widok.
// Parametry do widoku przekazujemy przez zmienne.

//ochrona kontrolera - poniższy skrypt przerwie przetwarzanie w tym punkcie gdy użytkownik jest niezalogowany
include _ROOT_PATH.'/app/security/check.php';

//pobranie parametrów
function getParams(&$how_many_years,&$amount,&$percentage){
	$how_many_years = isset($_REQUEST['how_many_years']) ? $_REQUEST['how_many_years'] : null;
	$amount = isset($_REQUEST['amount']) ? $_REQUEST['amount'] : null;
	$percentage = isset($_REQUEST['percentage']) ? $_REQUEST['percentage'] : null;	
}

//walidacja parametrów z przygotowaniem zmiennych dla widoku
function validate(&$how_many_years,&$amount,&$percentage,&$messages){
	// sprawdzenie, czy parametry zostały przekazane
	if ( ! (isset($how_many_years) && isset($amount) && isset($percentage))) {		
		// sytuacja wystąpi kiedy np. kontroler zostanie wywołany bezpośrednio - nie z formularza
		// teraz zakładamy, ze nie jest to błąd. Po prostu nie wykonamy obliczeń
		return false;
	}

	// sprawdzenie, czy potrzebne wartości zostały przekazane
	if ( $how_many_years == "") {
		$messages [] = 'Nie podano lat';
	}
	if ( $amount == "") {
		$messages [] = 'Nie podano kwoty';
	}
	if ( $percentage == "") {
		$messages [] = 'Nie podano oprocentowania';
	}
	//nie ma sensu walidować dalej gdy brak parametrów
	if (count ( $messages ) != 0) return false;
	
	// sprawdzenie, czy zmienne są liczbami
	if (! is_numeric( $how_many_years )) {
		$messages [] = 'Lata nie są liczbą!';
	}

	if (! is_numeric( $amount )) {
		$messages [] = 'Kwota nie jest liczbą!';
	}	

	if (! is_numeric( $percentage )) {
		$messages [] = 'Kwota nie jest liczbą!';
	}	

	if (count ( $messages ) != 0) return false;
	else return true;
}


function process(&$how_many_years,&$amount,&$percentage,&$messages,&$result){
	global $role;
	
	//konwersja parametrów
	$how_many_years = intval($how_many_years);
	$amount = floatval($amount);
	$percentage = floatval($percentage);
	//wykonanie operacji
    if ($amount > 1000)

    {
        if ($role == 'admin'){
            $result = ($amount * (100+$percentage)/100) / ($how_many_years*12);}
        else {
            $messages [] = 'Tylko administrator może obliczać kwoty powyzej 1000!';
            $result = 0;
        }
    }
    else {
        $result = ($amount * (100+$percentage)/100) / ($how_many_years*12);
    }
}

//definicja zmiennych kontrolera
$how_many_years = null;
$amount = null;
$percentage = null;
$result = null;
$messages = array();

//pobierz parametry i wykonaj zadanie jeśli wszystko w porządku
getParams($how_many_years,$amount,$percentage);
if ( validate($how_many_years,$amount,$percentage,$messages) ) { // gdy brak błędów
	process($how_many_years,$amount,$percentage,$messages,$result);
}

// Wywołanie widoku z przekazaniem zmiennych
// - zainicjowane zmienne ($messages,$x,$y,$operation,$result)
//   będą dostępne w dołączonym skrypcie
include 'calc_view.php';