<?php require_once dirname(__FILE__) .'/../config.php';?>
<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl" lang="pl">
<head>
<meta charset="utf-8" />
<title>Kalkulator</title>
</head>
<body>

<?php if (!isset($how_many_years)) {
	$how_many_years="";}?>
<?php if (!isset($amount)) {
	$amount="";}?>
<?php if (!isset($percentage)) {
	$percentage="10";}?>

<form action="<?php print(_APP_URL);?>/app/calc.php" method="post">
	<label for="id_x">Ile lat?: </label>
	<input id="id_x" type="text" name="how_many_years" value="<?php print($how_many_years); ?>" /><br />
	<label for="id_y">Kwota: </label>
	<input id="id_y" type="text" name="amount" value="<?php print($amount); ?>" /><br />
	<label for="id_z">Oprocentowanie: </label>
	<input id="id_z" type="text" name="percentage" value="<?php print($percentage); ?>" /><br />
	
	<input type="submit" value="Oblicz" />
</form>	

<?php
//wyświeltenie listy błędów, jeśli istnieją
if (isset($messages)) {
	if (count ( $messages ) > 0) {
		echo '<ol style="margin: 20px; padding: 10px 10px 10px 30px; border-radius: 5px; background-color: #f88; width:300px;">';
		foreach ( $messages as $key => $msg ) {
			echo '<li>'.$msg.'</li>';
		}
		echo '</ol>';
	}
}
?>

<?php if (isset($result)){ ?>
<div style="margin: 20px; padding: 10px; border-radius: 5px; background-color: lightgreen; width:300px;">
<?php printf('Rata miesięczna to: %.2f',$result); ?>
</div>
<?php } ?>

</body>
</html>