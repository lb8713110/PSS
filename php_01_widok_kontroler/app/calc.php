<?php
// KONTROLER strony kalkulatora
require_once dirname(__FILE__).'/../config.php';

// W kontrolerze niczego nie wysyła się do klienta.
// Wysłaniem odpowiedzi zajmie się odpowiedni widok.
// Parametry do widoku przekazujemy przez zmienne.

// 1. pobranie parametrów

$how_many_years = $_REQUEST ['how_many_years'];
$amount = $_REQUEST ['amount'];
$percentage = $_REQUEST ['percentage'];

// 2. walidacja parametrów z przygotowaniem zmiennych dla widoku

// sprawdzenie, czy parametry zostały przekazane
if ( ! (isset($how_many_years) && isset($amount) && isset($percentage))) {
	//sytuacja wystąpi kiedy np. kontroler zostanie wywołany bezpośrednio - nie z formularza
	$messages [] = 'Błędne wywołanie aplikacji. Brak jednego z parametrów.';
}

// sprawdzenie, czy potrzebne wartości zostały przekazane
if ( $how_many_years == "") {
	$messages [] = 'Nie podano lat';
}
if ( $amount == "") {
	$messages [] = 'Nie podano kwoty';
}
if ( $percentage == "") {
	$messages [] = 'Nie podano oprocentowania';
}

//nie ma sensu walidować dalej gdy brak parametrów
if (empty( $messages )) {
	
	// sprawdzenie, czy $x i $y są liczbami całkowitymi
	if (! is_numeric( $how_many_years )) {
		$messages [] = 'Lata nie są liczbą!';
	}
	
	if (! is_numeric( $amount )) {
		$messages [] = 'Kwota nie jest liczbą!';
	}	

	if (! is_numeric( $percentage )) {
		$messages [] = 'Kwota nie jest liczbą!';
	}	


}

// 3. wykonaj zadanie jeśli wszystko w porządku

if (empty ( $messages )) { // gdy brak błędów
	
	//konwersja parametrów na int
	$how_many_years = intval($how_many_years);
	$amount = floatval($amount);
	$percentage = floatval($percentage);
	
	$result = ($amount * (100+$percentage)/100) / ($how_many_years*12);
}

// 4. Wywołanie widoku z przekazaniem zmiennych
// - zainicjowane zmienne ($messages,$x,$y,$operation,$result)
//   będą dostępne w dołączonym skrypcie
include 'calc_view.php';