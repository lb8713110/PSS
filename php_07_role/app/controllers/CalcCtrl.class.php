<?php
// W skrypcie definicji kontrolera nie trzeba dołączać już niczego.
// Kontroler wskazuje tylko za pomocą 'use' te klasy z których jawnie korzysta
// (gdy korzysta niejawnie to nie musi - np używa obiektu zwracanego przez funkcję)

// Zarejestrowany autoloader klas załaduje odpowiedni plik automatycznie w momencie, gdy skrypt będzie go chciał użyć.
// Jeśli nie wskaże się klasy za pomocą 'use', to PHP będzie zakładać, iż klasa znajduje się w bieżącej
// przestrzeni nazw - tutaj jest to przestrzeń 'app\controllers'.

// Przypominam, że tu również są dostępne globalne funkcje pomocnicze - o to nam właściwie chodziło

namespace app\controllers;

//zamieniamy zatem 'require' na 'use' wskazując jedynie przestrzeń nazw, w której znajduje się klasa
use app\forms\CalcForm;
use app\transfer\CalcResult;

/** Kontroler kalkulatora
 * @author Przemysław Kudłacik
 *
 */
class CalcCtrl {

	private $form;   //dane formularza (do obliczeń i dla widoku)
	private $result; //inne dane dla widoku
    private $hide_intro; //zmienna informująca o tym czy schować intro


    /**
	 * Konstruktor - inicjalizacja właściwości
	 */
	public function __construct(){
		//stworzenie potrzebnych obiektów
		$this->form = new CalcForm();
		$this->result = new CalcResult();
        $this->hide_intro = false;
	}
	
	/** 
	 * Pobranie parametrów
	 */
	public function getParams(){
        $this->form->how_many_years = getFromRequest('how_many_years');
        $this->form->amount = getFromRequest('amount');
        $this->form->percentage = getFromRequest('percentage');
	}
	
	/** 
	 * Walidacja parametrów
	 * @return true jeśli brak błedów, false w przeciwnym wypadku 
	 */
	public function validate() {
		// sprawdzenie, czy parametry zostały przekazane
        if (! (isset ( $this->form->how_many_years ) && isset ( $this->form->amount ) && isset ( $this->form->percentage ))) {
            // sytuacja wystąpi kiedy np. kontroler zostanie wywołany bezpośrednio - nie z formularza
            return false; //zakończ walidację z błędem
        } else {
            $this->hide_intro = true; //przyszły pola formularza, więc - schowaj wstęp
        }

        // sprawdzenie, czy potrzebne wartości zostały przekazane
        if ($this->form->how_many_years == "") {
            getMessages()->addError('Nie podano lat');
        }
        if ($this->form->amount == "") {
            getMessages()->addError('Nie podano kwoty');
        }
        if ($this->form->percentage == "") {
            getMessages()->addError('Nie podano oprocentowania');
        }

        // nie ma sensu walidować dalej gdy brak parametrów
        if (! getMessages()->isError()) {

            // sprawdzenie, czy $x i $y są liczbami całkowitymi
            if (! is_numeric ( $this->form->how_many_years )) {
                getMessages()->addError('Lata nie są liczbą!');
            }

            if (! is_numeric ( $this->form->amount )) {
                getMessages()->addError('Kwota nie jest liczbą!');
            }

            if (! is_numeric ( $this->form->percentage )) {
                getMessages()->addError('Procenty nie są liczbą!');
            }
        }

        return ! getMessages()->isError();
    }
	
	/** 
	 * Pobranie wartości, walidacja, obliczenie i wyświetlenie
	 */
	public function process(){

		$this->getParams();
		
		if ($this->validate()) {
				
			//konwersja parametrów na int
            $this->form->how_many_years = intval($this->form->how_many_years);
            $this->form->amount = floatval($this->form->amount);
            $this->form->percentage = floatval($this->form->percentage);
            getMessages()->addInfo('Parametry poprawne.');
//					if (inRole('admin')) {
            //wykonanie operacji
            if ($this->form->amount > 1000)
            {
                if (inRole('admin'))
                {
                    $this->result->result = ($this->form->amount * (100+$this->form->percentage)/100) / ($this->form->how_many_years*12);
                    getMessages()->addInfo('Wykonano obliczenia.');
                }
                    else
                    {
                        getMessages()->addError('Uprawnienia administratora konieczne.');
                    }
            }
            else
            {
                $this->result->result = ($this->form->amount * (100+$this->form->percentage)/100) / ($this->form->how_many_years*12);
                getMessages()->addInfo('Wykonano obliczenia.');
            }


        }

        $this->generateView();
    }
	
	
	/**
	 * Wygenerowanie widoku
	 */
	public function generateView(){
		//nie trzeba już tworzyć Smarty i przekazywać mu konfiguracji i messages
		// - wszystko załatwia funkcja getSmarty()
        getSmarty()->assign('user',unserialize($_SESSION['user']));
		getSmarty()->assign('page_title','Kalkulator Kredytowy 07');
		getSmarty()->assign('page_description','Role');
		getSmarty()->assign('page_header','Kontroler główny');
					
		getSmarty()->assign('form',$this->form);
		getSmarty()->assign('res',$this->result);
        getSmarty()->assign('hide_intro',$this->hide_intro);

		getSmarty()->display('CalcView.tpl'); // już nie podajemy pełnej ścieżki - foldery widoków są zdefiniowane przy ładowaniu Smarty
	}
}
